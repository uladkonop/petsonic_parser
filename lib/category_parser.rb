# frozen_string_literal: true

require_relative 'logger'
require_relative 'page_loader'
require_relative 'product_page_parser'

class CategoryParser
  attr_reader :base_url

  def initialize(base_url)
    @base_url = base_url
  end

  def run
    product_urls = extract_category
    parse_products(product_urls)
  end

  private

  def extract_category
    Logger.log("Loading page from #{base_url}")
    doc = load_page(base_url)

    check_category_existance!(doc)

    Logger.log('Extracting product urls from page')
    product_urls = extract_product_urls(doc)

    page_number = 2
    while check_paginate_button(doc)
      doc = load_page("#{base_url}?p=#{page_number}")
      product_urls += extract_product_urls(doc)
      page_number += 1
    end
    product_urls
  end

  def check_paginate_button(doc)
    doc.at_xpath('//button[@class="loadMore next button lnk_view btn btn-default"]')
  end

  def parse_products(product_urls)
    Logger.log('Start parsing products')
    product_urls
      .map { |url| ProductPageParser.new(url).run }
      .flatten
  end

  def extract_product_urls(doc)
    doc.xpath('//div[@class="product-desc display_sd"]/a').map { |a| a.attr(:href) }
  end

  def check_category_existance!(doc)
    no_category_node = doc.at_xpath('//div[@class="alert alert-danger"]')
    return if no_category_node.nil?

    Logger.log('No such category! Exit')
    exit
  end

  def load_page(url)
    PageLoader.load(url)
  end
end
