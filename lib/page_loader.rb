# frozen_string_literal: true

require 'curb'
require 'nokogiri'

require_relative 'logger'

class PageLoader
  def self.load(url)
    curl = Curl::Easy.new(url)

    headers = {}
    headers['User-Agent'] = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0'
    headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    curl.headers = headers

    curl.perform

    Nokogiri::HTML(curl.body_str)
  rescue StandardError => e
    Logger.log("Page load failed - url: #{url} \n(#{e})")
  end
end
