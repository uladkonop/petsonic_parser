# frozen_string_literal: true

class Logger
  def self.log(msg)
    puts msg
  end
end
