# frozen_string_literal: true

require_relative 'logger'
require_relative 'page_loader'

class ProductPageParser
  attr_reader :url

  def initialize(url)
    @url = url
  end

  def run
    parse_page
  end

  private

  def parse_page
    doc = load_page
    title = doc.xpath('//h1[@class="product_main_name"]').text
    Logger.log("Parsing product:#{title}")
    product_labels = doc.xpath('//ul[@class="attribute_radio_list pundaline-variations"]/li')
    img_url = doc.at_xpath('//img[@id="bigpic"]')&.attr(:src)
    Logger.log("Add image url: #{img_url}")
    count = 0

    product_labels.map do |node|
      product = {}

      weight = parse_weight(node)
      product[:title] = "#{title} - #{weight}"
      product[:price] = parse_price(node)
      Logger.log("Add product price: #{product[:price]}\n\n")
      product[:img_url] = img_url
      count += 1
      product
    end
  end

  def parse_weight(node)
    node.at_xpath('.//span[@class="radio_label"]').text
  end

  def parse_price(node)
    raw_price = node.at_xpath('.//span[@class="price_comb"]').text
    extract_digits_regexp = /\d+\.?\d*/
    extract_digits_regexp.match(raw_price)[0].to_f
  end

  def load_page
    PageLoader.load(url)
  end
end
