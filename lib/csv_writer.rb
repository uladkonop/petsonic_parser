# frozen_string_literal: true

require 'csv'

class CSVWriter
  HEADERS = %w[Name Price Image].freeze

  attr_reader :products, :filename

  def initialize(products, filename)
    @products = products
    @filename = filename
  end

  def run
    log_info
    CSV.open(filename, 'w') do |csv|
      csv << HEADERS
      products.each do |product|
        csv << [product[:title], product[:price], product[:img_url]]
      end
    end
  end

  private

  def log_info
    Logger.log("Writing output to the file: #{filename}")
  end
end
