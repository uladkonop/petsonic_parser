#! /usr/bin/ruby
# frozen_string_literal: true

require_relative 'lib/category_parser'
require_relative 'lib/csv_writer'

if __FILE__ == $PROGRAM_NAME

  if ARGV.size < 2
    Logger.log('Please, run script in the following way:
      "./runner.rb https://your_url.com output_filename.csv"')
    exit
  end

  url = ARGV[0]
  filename = ARGV[1]
  products = CategoryParser.new(url).run
  CSVWriter.new(products, filename).run
end
